from Bio import SeqIO
import re, sys
from os.path import basename

input_file = sys.argv[1]
fasta_sequences = SeqIO.parse(open(input_file),'fasta')

for fasta in fasta_sequences:
    geneid, description, sequence = fasta.id, fasta.description, str(fasta.seq)
    reformatted_line = geneid + "\t" + sequence
    if 'protein' in description:
        print(reformatted_line)