#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import numpy as np
import pandas as pd
import pickle
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # or any {'0', '1', '2'}

import matplotlib.pyplot as plt
import time
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import (
    Conv1D,
    ZeroPadding2D,
    Activation,
    Input,
    concatenate,
)
from tensorflow.keras.models import Model
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import GlobalMaxPool1D, GlobalAvgPool1D, MaxPooling1D
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import Lambda, Flatten, Dense, ReLU
from tensorflow.keras.initializers import glorot_uniform
from tensorflow.keras.layers import Layer
from tensorflow.keras.regularizers import l2
from tensorflow.keras.utils import Sequence
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from tensorflow.keras import activations
import tensorflow.keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import numpy.random as rng
from common_funcs_2 import pad_seqs_to_onehot
import common_funcs_2 as cf
import argparse
import json
import utils
import model_def
from df_loader import loader

"""
........................................................................................
    Main
........................................................................................
"""
physical_devices = tf.config.list_physical_devices("GPU")
print("Num GPUs detected:", len(physical_devices))
print(physical_devices)

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument(
    "-p",
    "--checkpoint",
    nargs="?",
    const="",
    help="timestamp of checkpoint to load.",
)

parser.add_argument(
    "-e",
    "--epochs",
    nargs="?",
    const=5,
    default=5,
    type=int,
    help="Epochs",
)

parser.add_argument(
    "-c",
    "--config",
    nargs="?",
    const="config.json",
    default="config.json",
    help="name of the config file ",
)

args = parser.parse_args()

with open(args.config, "r") as f:
    p = json.load(f)

time_stamp = cf.date_stamp()
p["date_time"] = time_stamp
p["config_file_name"] = "save/configs/" + p["date_time"] + ".json"

SCRATCH_PATH = p["SCRATCH_PATH"]
DATA_PATH = SCRATCH_PATH + "/ML/struct/"
SAVE_PLOTS_DIR = SCRATCH_PATH + "/ML/struct/save/plots/"
SAVE_WEIGHTS_DIR = SCRATCH_PATH + "/ML/struct/save/weights/"
CHECKPOINT_DIR = SCRATCH_PATH + "/ML/struct/save/chkpt/"
FILTERING = True


"""
   Read the training df
"""
df_train_and_val, df_double_holdout = loader(p)

df_train, df_validation = train_test_split(
    df_train_and_val, test_size=0.010, random_state=425364
)
if p["SAMPLE_FROM_TRAINING_SET"]:
    df_train = df_train.sample(p["TRAINING_SAMPLE_SIZE"], random_state=998877)

print("Training set size:", len(df_train))
print("Holdout set size (double-holdouts):", len(df_double_holdout))
print("Validation set (no-holdouts)", len(df_validation))
p = utils.update_p(p, df_train_and_val, df_double_holdout)

"""
    Prepare for training
"""
log_dir = "save/tboard/" + time_stamp
checkpoint_path = CHECKPOINT_DIR + time_stamp + ".ckpt"

tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)
cp_callback = ModelCheckpoint(
    filepath=checkpoint_path, save_weights_only=True, verbose=1
)
es_callback = EarlyStopping(
    monitor="val_loss", mode="auto", verbose=1, patience=3, min_delta=0.005
)

if not FILTERING:
    nplen = np.vectorize(len)
    all_seqs = np.append(df_train_and_val.qseq.values, df_train_and_val.tseq.values)
    max_seq_len = np.max(nplen(all_seqs))
else:
    max_seq_len = p["MAX_SEQ_LEN"]

padded_len = max_seq_len + 10

if p["MODEL"] == "RESNET32":
    model = model_def.get_siamese_model(p, padded_len)
elif p["MODEL"] == "SIMPLE":
    model = model_def.define_simple_model(p, padded_len)

optimizer = Adam(lr=0.0005)
model.compile(loss=p["LOSS"], optimizer=optimizer, metrics=["mse", "mae"])
n_params = model.count_params()
print("-" * 40)
print("Model has {} parameters.".format(n_params))
model.summary()

if args.checkpoint:
    checkpoint_path = CHECKPOINT_DIR + args.checkpoint + ".ckpt"
    model.load_weights(checkpoint_path)
    print("Loadaed checkpint", args.checkpoint)
    print("Running for ", args.epochs, "epochs.")

epochs = args.epochs
p["epochs"] = epochs
bpe = None
if p["BATCHES_PER_EPOCH"]:
    bpe = p["BATCHES_PER_EPOCH"]


"""
    Train
"""
train_gen = model_def.DataGenerator(
    df_train_and_val,
    padded_len,
    max_seq_len,
    batch_size=128,
    batches_per_epoch=bpe,
    include_weights=p["WEIGHTED_SAMPLES"],
)
validation_gen = model_def.DataGenerator(
    df_double_holdout,
    padded_len,
    max_seq_len,
    batch_size=128,
    include_weights=p["WEIGHTED_SAMPLES"],
)


history = model.fit(
    train_gen,
    epochs=epochs,
    verbose=1,
    use_multiprocessing=False,
    validation_data=validation_gen,
    callbacks=[
        cp_callback,
    ],
)


"""
    Plot results
"""

model.save_weights(SAVE_WEIGHTS_DIR + time_stamp + "-weights.hdf5")

# HOLDOUT_SAMPLE_SIZE = 10000
# df_sample = df_double_holdout.sample(n=HOLDOUT_SAMPLE_SIZE)
# df_sample = df_double_holdout
utils.plot_predictions(
    p,
    model,
    df_validation,
    padded_len,
    max_seq_len,
    SAVE_PLOTS_DIR,
    sample_size=5000,
    title_prefix="Validation(no holdouts)",
    file_prefix="no_holdout_val",
)
utils.plot_predictions(
    p,
    model,
    df_double_holdout,
    padded_len,
    max_seq_len,
    SAVE_PLOTS_DIR,
    sample_size=5000,
    title_prefix="Double-holdouts)",
    file_prefix="double_holdouts",
)
utils.plot_hist(history, p, val_loss=True, scale="log")
utils.save_config(p)
