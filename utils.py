import numpy as np
import pandas as pd

# import dovpanda
from numpy import mean
from numpy import std

import matplotlib

matplotlib.use("Agg")

from matplotlib import pyplot
from sklearn.model_selection import KFold
import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import concatenate
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.callbacks import EarlyStopping
from sklearn import metrics
from common_funcs_2 import pad_seqs_to_onehot
from common_funcs_2 import date_stamp
import matplotlib.pyplot as plt
import umap
from os import path
import time
import json
from shutil import copyfile
import scipy


def update_p(p, df_train_and_val, df_double_holdout):
    p["n_selected_sequences"] = len(df_train_and_val) + len(df_double_holdout)
    p["n_validation_sequences"] = len(df_double_holdout)
    p["n_training_sequences"] = len(df_train_and_val)
    p["n_single_holdout_sequences"] = -1
    p["n_double_holdout_sequences"] = len(df_double_holdout)
    # df_train_and_val = df_train_and_val.sample(100000, random_state=424242)
    return p


def isnotebook():
    try:
        shell = get_ipython().__class__.__name__
        if shell == "ZMQInteractiveShell":
            return True  # Jupyter notebook or qtconsole
        elif shell == "TerminalInteractiveShell":
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False  # Probably standard Python interpreter


# ## Define plot_hist

# In[4]:


def reduce_dim(
    model,
    df_to_embed,
    embed_file_name,
    padded_len,
    max_seq_len,
    twoD=False,
    center_padding=True,
):

    print("Reducing dim...", flush=True)
    df = df_to_embed.copy().reset_index(drop=True)
    seqs = df.seq

    seqs_onehot = pad_seqs_to_onehot(
        seqs,
        padded_len,
        max_seq_len,
        random_padding=False,
        center_padding=center_padding,
    )

    # float16 greatly reduces df saving time.
    start = time.time()
    representations = model.predict(seqs_onehot)
    representations = [np.float16(i) for i in representations]
    elapsed = time.time() - start
    print("Generated representations in {:.2f} secs".format(elapsed), flush=True)

    # df['representations'] = representations
    start = time.time()
    if twoD:
        reducer = umap.UMAP(init="random", random_state=42434445)

        embeddings = reducer.fit_transform(representations)
        df_embedding = pd.DataFrame(embeddings, columns=("x", "y"))

    else:
        df_embedding = pd.DataFrame(
            np.zeros((len(seqs), 2), dtype=np.float16), columns=("x", "y")
        )

    elapsed = time.time() - start
    print("Generated umap epmbeddings in {:.2f} secs".format(elapsed), flush=True)

    df_embedding["foldNumber"] = df["foldNumber"].values  # ignore index
    df_embedding["seq"] = df["seq"]
    df_embedding["representation"] = representations
    df_embedding["validation_set"] = df["validation_set"]

    if "accession" in df.columns:
        df_embedding["pfam_accession"] = df["accession"]
    else:
        df_embedding["pfam_accession"] = df["pfam_accession"]

    print("Saving representations...", flush=True)
    start = time.time()
    df_embedding.to_pickle(embed_file_name)
    elapsed = time.time() - start
    print("Time elapsed: {:.2f}".format(elapsed))
    print("Saved representations to ", embed_file_name, flush=True)

    return df_embedding


# ## Define plot_hist

# In[4]:

import matplotlib.pyplot as plt
from scipy import stats


def plot_hist(history, p, val_loss=False, scale=None):
    print("Plotting history...", flush=True)
    num_epochs = len(history.history["loss"])
    x_values = range(1, num_epochs + 1)
    # A = history.history['loss'][0]
    # exponent = -1

    fig = plt.figure()
    ax = fig.add_subplot(111)
    # guide = [A * (x**exponent) for x in x_values]

    X = np.log(x_values)
    y = np.log(history.history["loss"])
    exponent, intercept, r_value, p_value, std_err = stats.linregress(X, y)
    A = np.exp(intercept)
    print("exponent: %.2f\tA: %.2f" % (exponent, A))

    fig.patch.set_facecolor("white")
    plt.plot(x_values, history.history["loss"], "x-")
    fit_label = "x^{:.2f}".format(exponent)
    plt.plot(x_values, A * (x_values ** exponent), "r")
    plt.plot(x_values, history.history["val_mae"], "+-")

    if val_loss:
        plt.plot(x_values, history.history["val_loss"], "o-")
        plt.legend(["Train MSE", fit_label, "Val MAE", "Val Loss"], loc="upper right")
    else:
        plt.legend(["Train MSE", fit_label, "Val MAE"], loc="upper right")

    plt.title("Loss")
    plt.ylabel("Loss")
    plt.xlabel("Epoch")

    if scale == "log":
        ax.set_yscale("log")

    elif scale == "loglog":
        ax.set_yscale("log")
        ax.set_xscale("log")

    if scale == None:
        scale = "lin"

    prefix = "History_"
    out_path = "./save/history/"

    if path.exists(out_path):
        prefix = out_path + prefix

    file_name = prefix + scale + "_" + p["date_time"] + ".png"
    plt.savefig(file_name, format="png", pad_inches=0.3)

    # Write the history to text file
    text_file_name = prefix + "_" + p["date_time"] + ".tsv"
    with open(text_file_name, "w+") as f:
        f.write("t\tLoss\tVal_Loss\n")
        for i, t in enumerate(x_values):
            f.write("{:d}\t{:.4f}".format(t, history.history["loss"][i]))

            if val_loss:
                f.write("\t{:.4f}".format(history.history["val_loss"][i]))

            f.write("\n")

    print("Saved history plot to file:", file_name, flush=True)

    if isnotebook():
        plt.show()


def save_config(p):
    with open(p["config_file_name"], "w") as json_file:
        json.dump(p, json_file, indent=4)
        print("Saved config to", p["config_file_name"])

    copyfile("model_def.py", "save/configs/model_def_" + p["date_time"] + ".py")


def plot_concordance(
    tmalign_distance,
    predicted_distance,
    distance_ratio,
    len_ratio,
    time_stamp,
    SAVE_PLOTS_DIR,
    parameters,
    title_prefix,
    file_prefix,
):
    R, p = scipy.stats.pearsonr(tmalign_distance, predicted_distance)
    title = "{}: Pearson R={:.2f}\n{}".format(title_prefix, R, time_stamp)

    fig = plt.figure(figsize=(6, 6))
    fig.patch.set_facecolor("white")
    plt.axis((0.0, 1, 0.0, 1))
    plt.scatter(tmalign_distance, predicted_distance, s=0.1)
    plt.plot([0, 1], [0, 1], color="red")
    plt.title(title)
    plt.xlabel("Actual Distance")
    plt.ylabel("Predicted Distance")
    file_name = f"{SAVE_PLOTS_DIR}{time_stamp}-{file_prefix}-actual_vs_predicted.png"
    plt.savefig(file_name, format="png", pad_inches=0.3)

    # In[ ]:

    diff = np.array(tmalign_distance - predicted_distance)
    mean = np.average(diff)
    stdev = np.std(diff)
    title = "{} Mean={:.2f}, Stdev={:.2f}\n{}".format(
        title_prefix, mean, stdev, time_stamp
    )
    fig = plt.figure(figsize=(6, 6))
    fig.patch.set_facecolor("white")
    # plt.axis((0.5, 1, 0.5, 1))
    plt.hist(diff, bins=25)
    plt.title(title)
    plt.xlabel("tmalign - predicted distance")
    plt.ylabel("Count")
    file_name = (
        f"{SAVE_PLOTS_DIR}{time_stamp}-{file_prefix}-distance_deviation_dist.png"
    )
    plt.savefig(file_name, format="png", pad_inches=0.3)

    print("R={:.2f}, mean={:.2f}, stdev={:.2f}".format(R, mean, stdev))
    parameters["Pearson_R"] = "{:.2f}".format(R)
    parameters["mean"] = "{:.2f}".format(mean)
    parameters["stdev"] = "{:.2f}".format(stdev)

    R, p = scipy.stats.pearsonr(distance_ratio, len_ratio)
    title = "{}: Pearson R={:.2f}\n{}".format(title_prefix, R, time_stamp)
    fig = plt.figure(figsize=(6, 6))
    fig.patch.set_facecolor("white")

    # plt.axis((0.5, 1, 0.5, 1))
    plt.scatter(distance_ratio, len_ratio, s=1)
    plt.title(title)
    plt.xlabel("tmalign_distance / predicted_distance")
    plt.ylabel("q_len/t_len")
    file_name = (
        f"{SAVE_PLOTS_DIR}{time_stamp}-{file_prefix}-length_diff_vs_deviation.png"
    )
    plt.savefig(file_name, format="png", pad_inches=0.3)

    return parameters
    # In[ ]:


def plot_predictions(
    p,
    model,
    df,
    padded_len,
    max_seq_len,
    save_path,
    sample_size=5000,
    title_prefix="",
    file_prefix="",
):
    SAMPLE_SIZE_FOR_PLOTTING = len(df) if len(df) < sample_size else sample_size
    df_sample = df.sample(SAMPLE_SIZE_FOR_PLOTTING)
    q_seqs = df_sample.qseq.values.tolist()
    t_seqs = df_sample.tseq.values.tolist()
    tmalign_distance = df_sample.distance

    q_seqs_ohe = pad_seqs_to_onehot(
        q_seqs,
        padded_len,
        max_seq_len,
        random_padding=False,
        center_padding=True,
        dtype=np.int8,
    )

    t_seqs_ohe = pad_seqs_to_onehot(
        t_seqs,
        padded_len,
        max_seq_len,
        random_padding=False,
        center_padding=True,
        dtype=np.int8,
    )

    # float16 greatly reduces df saving time.
    start = time.time()
    predicted_distance = np.array(model.predict([q_seqs_ohe, t_seqs_ohe]).flatten())

    q_len = np.array([len(x) for x in q_seqs])
    t_len = np.array([len(x) for x in t_seqs])
    len_ratio = (q_len - t_len) / q_len
    distance_ratio = tmalign_distance / predicted_distance

    p = plot_concordance(
        tmalign_distance,
        predicted_distance,
        distance_ratio,
        len_ratio,
        p["date_time"],
        save_path,
        p,
        title_prefix=title_prefix,
        file_prefix=file_prefix,
    )
