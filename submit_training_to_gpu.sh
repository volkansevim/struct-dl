#!/bin/bash

#SBATCH --constraint=gpu
#SBATCH --nodes=1
#SBATCH --time=0:10:00
#SBATCH -c 2
#SBATCH --gpus=1
#SBATCH -A m342


#SBATCH -J traingpu
#SBATCH -o /global/projectb/scratch/vsevim/ML/struct/save/logs/%x-%j.out

# Setup software
conda activate gpu
module load esslurm
#module purge
#module load cgpu
#module load gcc cuda openmpi



# Run the training
srun -u python ./struct_dl_00.py -e 100 -c cori_config.json


