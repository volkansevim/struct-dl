fgrep "_exptl.method " ../cif/*.cif > METHOD.TXT

cat METHOD.TXT | tr "/" "\t" | \
	tr "." "\t" | tr -s " " | \
	sed -r 's/[ ]+/\t/'| sed 's/method /\t/' | \
	tr -s "\t" | cut -f3,6 | tr -d "'" | \
	sed -r 's/[ ]+$//' > PDB_ID_vs_METHOD.TSV

mv METHOD.TXT .backup/