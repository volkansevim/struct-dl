#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import numpy as np
import pandas as pd
import pickle
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # or any {'0', '1', '2'}

import matplotlib.pyplot as plt
import time
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import (
    Conv1D,
    ZeroPadding2D,
    Activation,
    Input,
    concatenate,
)
from tensorflow.keras.models import Model
from tensorflow.keras.layers import BatchNormalization, Dropout
from tensorflow.keras.layers import GlobalMaxPool1D, GlobalAvgPool1D, MaxPooling1D
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import Lambda, Flatten, Dense, ReLU
from tensorflow.keras.initializers import glorot_uniform
from tensorflow.keras.layers import Layer
from tensorflow.keras.regularizers import l1, l2
from tensorflow.keras.utils import Sequence
from tensorflow.keras import activations
import tensorflow.keras.backend as K
from sklearn.utils import shuffle
import numpy.random as rng
from common_funcs_2 import pad_seqs_to_onehot
import common_funcs_2 as cf
import argparse
import json


class DataGenerator(Sequence):
    def __init__(
        self,
        df,
        padded_len,
        max_seq_len,
        batch_size=32,
        batches_per_epoch=None,
        shuffle=True,
        include_weights=False,
    ):

        """
        Initialization
        """
        self.df = df
        """ 
            Each sample is repeated k times to create 
            variationally padded self-self pairs.. 
        """
        self.shuffle = shuffle
        self.padded_len = padded_len
        self.max_seq_len = max_seq_len
        self.epoch = 1
        self.batch_size = batch_size
        self.batch_count = 0
        if batches_per_epoch:
            self.batches_per_epoch = batches_per_epoch
        else:
            self.batches_per_epoch = int(len(df) / batch_size)
        self.on_epoch_end()
        self.include_weights = include_weights

    def __len__(self):
        #'Denotes the number of batches per epoch'
        return self.batches_per_epoch

    def __getitem__(self, index):
        selected_records = []
        self.batch_count += 1

        if self.batch_count >= self.batches_per_epoch:
            self.batch_count = 0
            self.epoch += 1

        df_batch = self.df.sample(self.batch_size)

        # np_len = np.vectorize(len)
        # all_seqs = np.append(df_batch.qseq.values, df_batch.tseq.values)
        # max_seq_len = np.max(np_len(all_seqs))
        # padded_len = max_seq_len + 10

        query_batch = pad_seqs_to_onehot(
            df_batch.qseq.values,
            self.padded_len,
            self.max_seq_len,
            random_padding=False,
            dtype=np.int8,
            center_padding=True,
        )

        target_batch = pad_seqs_to_onehot(
            df_batch.tseq.values,
            self.padded_len,
            self.max_seq_len,
            random_padding=False,
            dtype=np.int8,
            center_padding=True,
        )

        training_distances = df_batch.distance.values
        training_distances = training_distances.astype(np.float16)

        if self.include_weights:
            sample_weights = df_batch.weight.values.astype(np.float16)
            return [query_batch, target_batch], training_distances, sample_weights
        else:
            return [query_batch, target_batch], training_distances

    def on_epoch_end(self):
        pass

    def __data_generation(self, list_IDs_temp):
        pass


class ResidualUnit(Layer):
    def __init__(self, filters, strides=1, activation="relu", **kwargs):
        super().__init__(**kwargs)
        self.activation = activations.get(activation)
        self.main_layers = [
            Conv1D(filters, 3, strides=strides, padding="same", use_bias=False),
            BatchNormalization(),
            self.activation,
            Conv1D(filters, 3, strides=1, padding="same", use_bias=False),
            BatchNormalization(),
        ]

        self.skip_layers = []
        if strides > 1:
            self.skip_layers = [
                Conv1D(filters, 1, strides=strides, padding="same", use_bias=False),
                BatchNormalization(),
            ]

    def call(self, inputs):
        Z = inputs
        for layer in self.main_layers:
            Z = layer(Z)
        skip_Z = inputs
        for layer in self.skip_layers:
            skip_Z = layer(skip_Z)
        return self.activation(Z + skip_Z)


# In[14]:


def get_siamese_model(p, padded_len, output_dim=300):
    input_shape = (padded_len, 20)
    print(input_shape)

    # Define the tensors for the two input sequences
    left_input = Input(input_shape)
    right_input = Input(input_shape)

    # CNN
    model = Sequential()
    model.add(
        Conv1D(
            64, 3, strides=1, input_shape=input_shape, padding="same", use_bias=False
        )
    )
    model.add(BatchNormalization())
    model.add(Activation("relu"))
    model.add(MaxPooling1D(pool_size=2, strides=1, padding="same"))
    prev_filters = 64
    for filters in [64] * 3 + [128] * 4 + [256] * 6 + [512] * 3:
        strides = 1 if filters == prev_filters else 2
        model.add(ResidualUnit(filters, strides=strides))
        prev_filters = filters
    model.add(GlobalAvgPool1D())
    model.add(Flatten())
    model.add(BatchNormalization())
    model.add(
        Dense(
            output_dim,
            activation="linear",
            kernel_regularizer=l1(0.01),
            activity_regularizer=l2(1e-5),
        )
    )
    # model.add(Lambda(lambda x: tf.math.l2_normalize(x, axis=1)))

    # Generate the encodings (feature vectors) for the two images
    encoded_l = model(left_input)
    encoded_r = model(right_input)

    if p["DISTANCE_LAYER"].upper() == "L1":
        # Add a customized layer to compute the L1 distance between the encodings
        L1_layer = Lambda(lambda tensors: tf.keras.backend.abs(tensors[0] - tensors[1]))
        L1_distance = L1_layer([encoded_l, encoded_r])
        prediction = Dense(1, activation="linear")(L1_distance)

    elif p["DISTANCE_LAYER"].upper() == "L2":
        # Euclidean distance layer, and a dense layer with a linear unit
        # to generate the similarity score
        L2_layer = Lambda(
            lambda tensors: K.sqrt(
                K.sum(K.square(tensors[0] - tensors[1]), axis=1, keepdims=True)
            )
        )
        L2_distance = L2_layer([encoded_l, encoded_r])
        prediction = Dense(1, activation="linear")(L2_distance)

    # Connect the inputs with the outputs
    siamese_net = Model(inputs=[left_input, right_input], outputs=prediction)

    # return the model
    return siamese_net


def define_simple_model(p, padded_len):
    input_shape = (padded_len, 20)
    num_filters = p["NUM_FILTERS"]

    left_input = Input(input_shape)
    right_input = Input(input_shape)

    model = Sequential()
    model.add(
        Conv1D(
            num_filters,
            kernel_size=p["KERNEL_SIZE"],
            strides=1,
            input_shape=input_shape,
            padding="same",
            use_bias=True,
            activation="relu",
        )
    )

    model.add(GlobalMaxPool1D())
    model.add(Flatten())
    model.add(Dense(32, activation="relu", kernel_regularizer=l1(0.01)))
    model.add(Dropout(0.3))
    model.add(Dense(1, activation="linear"))
    # model.add(Dense(1, activation='linear', kernel_regularizer=l1(0.01), activity_regularizer=l2(1e-5)))

    encoded_l = model(left_input)
    encoded_r = model(right_input)

    if p["DISTANCE_LAYER"].upper() == "L1":
        # Add a customized layer to compute the L1 distance between the encodings
        L1_layer = Lambda(lambda tensors: tf.keras.backend.abs(tensors[0] - tensors[1]))
        L1_distance = L1_layer([encoded_l, encoded_r])
        prediction = Dense(1, activation="linear")(L1_distance)

    elif p["DISTANCE_LAYER"].upper() == "L2":
        # Eeuclidean distance layer, and a dense layer with a linear unit to generate the similarity score
        L2_layer = Lambda(
            lambda tensors: K.sqrt(
                K.sum(K.square(tensors[0] - tensors[1]), axis=1, keepdims=True)
            )
        )
        L2_distance = L2_layer([encoded_l, encoded_r])
        prediction = Dense(1, activation="linear")(L2_distance)

    elif p["DISTANCE_LAYER"].upper() == "COS":
        cos_layer = tf.keras.layers.Dot(axes=1, normalize=True)([encoded_l, encoded_r])
        dist_layer = Lambda(lambda x: 1.0 - x)(cos_layer)
        prediction = Dense(1, activation="linear")(dist_layer)

    # Connect the inputs with the outputs
    siamese_net = Model(inputs=[left_input, right_input], outputs=prediction)

    # return the model
    return siamese_net

    return model
