#!/bin/bash
#SBATCH -N 1
#SBATCH -C haswell
#SBATCH -q premium
#SBATCH -t 48:00:00
#SBATCH --exclusive
#SBATCH -J TMExclusive

# srun env -i HOME="$HOME" \
#     /bin/bash --noprofile --norc \
#     -c ./run_parallel_tmalign.sh

srun env -i HOME="$HOME" \
    /bin/sh -c ./run_parallel_tmalign.sh

echo "run_parallel_tmalign.sh completed."