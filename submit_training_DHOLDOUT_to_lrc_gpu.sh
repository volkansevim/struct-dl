#!/bin/bash

#SBATCH --job-name=struct-gpu
#SBATCH --partition=es1
#SBATCH --qos=es_normal
#SBATCH --account=ac_deeplife
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=2
#SBATCH --time=08:00:00
#SBATCH -o /global/scratch/vsevim/ML/struct/save/logs/%x-%j.LRC.out


#module load python/3.6
#source ~/.virtualenv/p36/bin/activate

module unload python/3.6
#module load python/3.7
#module load cuda/10.2
module load ml/tensorflow/2.1.0-py37 


# Run the training
srun -u python ./struct_dl_01_dholdout.py -e 5


