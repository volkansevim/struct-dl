#!/bin/bash

#SBATCH --job-name=struct-gpu
#SBATCH --partition=es1
#SBATCH --constraint es1_v100
#SBATCH --qos=es_normal
#SBATCH --account=ac_deeplife
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=2
#SBATCH --time=11:00:00
#SBATCH -o /global/scratch/vsevim/ML/struct/save/logs/%x-%j.LRC.out


#source ~/.virtualenv/p36/bin/activate
module purge
module load ml/tensorflow/2.1.0-py37 

# Check gpu types
nvidia-smi 

# Run the training
srun -u python ./struct_dl_00.py -e 5


