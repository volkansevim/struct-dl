#!/bin/bash

MAX_PARALLEL_PROC=4
if [[ -z $1 ]]; then
	echo USAGE: tmalign_pairs.sh pairs.tsv
	exit
fi

pairs_file="$1"
rm -rf temp
mkdir temp

while read -r pair; do
    (
        echo $pair
    )&
  
    NPROC=$(($NPROC+1))
    if [ "$NPROC" -ge "$MAX_PARALLEL_PROC" ]; then
      wait
      NPROC=0
    fi
  
done <<< "$pairs_file"


##~/software/tmalign/TMalign 4v7h.cif 3j6x.cif -split 2 -ter 0 -outfmt 2 -TMcut 0.5 > 4v7h_vs_3j6x.txt