#!/bin/bash
#SBATCH --qos=debug
#SBATCH --Nodes=2
#SBATCH --constraint=haswell
#SBATCH --ntasks-per-node 1

srun --no-kill --ntasks=2 --wait=0 payload.sh $1 
