#!/bin/bash
module load parallel
if [[ -z "${SLURM_NODEID}" ]]; then
    echo "need \$SLURM_NODEID set"
    exit
fi
if [[ -z "${SLURM_NNODES}" ]]; then
    echo "need \$SLURM_NNODES set"
    exit
fi
cat $1 |                                               \
awk -v NNODE="$SLURM_NNODES" -v NODEID="$SLURM_NODEID" 'NR % NNODE == NODEID' | \
	parallel
	    --resume \
	    --joblog JOB.LOG \
	    --colsep '\t' \
	    ./TMalign cif/{1}.cif cif/{3}.cif \
			-S1 {2} -S2 {4} \
	        -split 2 -ter 0 -outfmt 2 -noheader \
       		>>RESULTS_.TSV."$SLURM_NODEID" 2>>STDERR.LOG."$SLURM_NODEID" 

#	env -i HOME="$HOME" /bin/bash --noprofile --norc -c \
