machine="$(uname -s)"

if [[ "$machine" == "Linux" ]]; then
	echo Linux
	g++ -static -O3 -ffast-math -lm -o TMalign TMalign.cpp
elif [[ "$machine" == "Darwin" ]]; then
	echo MacOS
	# No -static on mac os
 	g++ -O3 -ffast-math -lm -o TMalign TMalign_mac.cpp
fi