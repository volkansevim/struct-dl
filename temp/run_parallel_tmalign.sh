if [[ $(nproc) -lt 65 ]]; then
    ~/bin/parallel \
        -a dummy_10K_pairs.tsv \
        --joblog joblog.log \
        --colsep '\t' \
        ./TMalign_HSW cif/{1}.cif cif/{2}.cif \
            -split 2 -ter 0 -outfmt 2 \
            > RESULTS.TXT 2>STDERR.LOG
else
    ~/bin/parallel \
        -a dummy_10K_pairs.tsv \
        --joblog joblog.log \
        --colsep '\t' \
        ./TMalign_KNL cif/{1}.cif cif/{2}.cif \
            -split 2 -ter 0 -outfmt 2 \
            > RESULTS.TXT 2>STDERR.LOG
fi