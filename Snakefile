MIN_LEN = 30
BLAST_NUM_CORES = 63
min_len_cov = .80
min_pid = 15


#cut -f1 -d"_" pdb_seqres.tsv | sort | sed 's/[a-z]$//' |  sort | uniq -c | sed -e 's/ *//' -e 's/ /\t/' | sort -nr > PDB_COUNTS.TSV

rule all:
    input:
        "nr_pdb_filtered.tsv",
        "nr_pdb_filtered.faa",
        "blastdb/pdb.phr",
        "blastdb/pdb.pin",
        "blastdb/pdb.psq",
        "nr_pdb_filtered.blast.tsv",
        "PDB_FILES.TXT",
        "PDB_PAIRS.TSV",
        "HIGH_SEQ_ID_PAIRS.pkl.gz",
        "STRUCTURAL_ALIGNMENT_SCORES.TSV",
        "SEQ_PAIRS.TSV",
        "scores.dummy.tsv",
        "SCORES.pkl.gz"        
        


rule get_pdb_fasta:
    output:
        "pdb_seqres.txt"
    shell:
        "wget ftp://ftp.wwpdb.org/pub/pdb/derived_data/pdb_seqres.txt.gz;"
        "gunzip pdb_seqres.txt.gz"



rule fasta_to_tsv:
    input:
        "pdb_seqres.txt"
    output:
        "pdb_seqres.tsv"
    shell:
        "python 'linearize_pdb_fasta.py' {input} > {output}"



rule remove_duplicates:
    input:
        "pdb_seqres.tsv"
    output:
        tsv = "nr_pdb_filtered.tsv",
        faa = "nr_pdb_filtered.faa",
        unfiltered_tsv = temp("all_lengths_nr_pdb.tsv")
    shell:
        # Remove redundants and short sequences
        "./REMOVE_REDUNDANCY.sh {input} {output.tsv} {output.faa} {output.unfiltered_tsv} {MIN_LEN}"



rule build_blast_db:
    input:
        "nr_pdb_filtered.faa"
    output:
        "blastdb/pdb.phr",
        "blastdb/pdb.pin",
        "blastdb/pdb.psq"
    shell:
        "module load blast+;"
        "makeblastdb -in {input} -title 'pdb' -dbtype prot -out 'blastdb/pdb'"



rule blast_the_faa:
    input:       
       "blastdb/pdb.phr",
       "blastdb/pdb.pin",
       "blastdb/pdb.psq",
       faa = "nr_pdb_filtered.faa"
    output:
        "nr_pdb_filtered.blast.tsv"
    shell:
        "./RUN_BLAST.sh {input.faa} {output} {BLAST_NUM_CORES}"


'''
    Read the blast output, find pairs above a certain length coverage
    and certain %ID. Write those into PDB_PAIRS.TSV"
''' 
rule generate_pdb_pairs:  
    input:
        NR = "nr_pdb_filtered.blast.tsv"
    output:
        "PDB_PAIRS.TSV",
        "PDB_FILES.TXT",
        "HIGH_SEQ_ID_PAIRS.pkl.gz"
    shell:
        "python ./GENERATE_PDB_PAIRS.py -f {input.NR}"
        " --min_cov {min_len_cov}"
        " --min_pid {min_pid}"
        

rule run_structual_alignment:  
    input:
        "PDB_PAIRS.TSV"
    output:
        "STRUCTURAL_ALIGNMENT_SCORES.TSV", 
        "STDERR.LOG",
        "JOB.LOG"
    shell:
        "echo 'Make sure you deleted JOB.LOG or gnu parallel will append to it'",
        "sbatch ./submit_tmalign.sh"


'''
    Structural alignment output does not include the sequences. Insert those
    into the df because they're needed for the training.
'''
rule assign_scores_to_seq_pairs:
    input:
        "STRUCTURAL_ALIGNMENT_SCORES.TSV"
    output:
        "SEQ_PAIRS.TSV",
        "scores.dummy.tsv"
    shell:
        "./assign_seqs_to_score_pairs.sh"



'''
    Write all training/holdout data to SCORES.pkl.gz. 
'''
rule create_training_df:
    input:
        "SEQ_PAIRS.TSV",
        "scores.dummy.tsv"
    output:
        "SCORES.pkl.gz"
    shell:
        "python ./create_training_df.py"
    
