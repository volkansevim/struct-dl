import pandas as pd
import numpy as np
import argparse
from termcolor import colored
import os


parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument(
    "-f",
    "--file",
    nargs="?",
    const="",
    default="",
    type=str,
    help="Minimum %%identity",
)

parser.add_argument(
    "-c",
    "--min_cov",
    nargs="?",
    const=0.8,
    default=0.8,
    type=float,
    help="minimum length coverage (0, 1)",
)

parser.add_argument(
    "-p",
    "--min_pid",
    nargs="?",
    const=30,
    default=30,
    type=int,
    help="Minimum %%identity",
)

args = parser.parse_args()


"""
   Main
"""
filename = args.file

print("Reading", filename)
df_nr = pd.read_csv(
    filename,
    sep="\t",
    names=(
        "qid",
        "tid",
        "pidentity",
        "aln_len",
        "mismatches",
        "qlen",
        "tlen",
        "perc_q_cov_per_hsp",
        "evalue",
        "bitscore",
    ),
    comment="#",
)

df_nr = df_nr[df_nr.qid != df_nr.tid]

qlen = df_nr["qlen"].values
tlen = df_nr["tlen"].values
qt_len_pairs = np.vstack((qlen, tlen)).transpose()

len_cov = np.array([min(x) / max(x) for x in qt_len_pairs])

MIN_LEN_COV = args.min_cov
MIN_PID = args.min_pid

# df_hi_pid contains the pairs that satisfy the minimum length coverage
# and minimum pid.

df_hi_pid = df_nr[(len_cov > MIN_LEN_COV) & (df_nr.pidentity > MIN_PID)]

print("{:,} pairs in df (all experimental methods)".format(len(df_hi_pid)))
df_hi_pid.to_pickle("HIGH_SEQ_ID_PAIRS.pkl.gz")

"""
    1. Write file names to download from pdb into file
"""
qids = df_hi_pid.qid.values
tids = df_hi_pid.tid.values


all_ids = np.append(qids, tids)
all_ids = np.array([x.split("_")[0] for x in all_ids])
all_ids = np.unique(all_ids)

print("Writing {} file names...".format(len(all_ids)))
with open("PDB_FILES.TXT", "w+") as f:
    for pdbid in all_ids:
        f.write("https://files.rcsb.org/download/{}.cif\n".format(pdbid))

print("Written PDB_FILES.TXT")


"""
    2. Now download run ./download_pdb_ids.sh to download all the .cif files
       Do this manually.
"""
print(
    colored(
        "\n\n!!! Running ./download_pdb_ids.sh to download necessary "
        ".cif files. Use snakemake -w 50000 to wait for this step to "
        "complete!!!\n\n",
        "red",
    )
)

if os.system("./download_pdb_ids.sh"):
    print("download_pdb_ids.sh failed")
    sys.exit(-1)

""" 
    3. Get experimental methods from the cif files. Exclude NMR files.
"""
if os.system("./GET_METHODS.sh"):
    print("GET_METHODS.sh failed")
    sys.exit(-1)


""" 
    4. Write valid pairs into a text file
"""
df_methods = pd.read_csv("PDB_ID_vs_METHOD.TSV", sep="\t", names=("PDB_ID", "method"))
xray_ids = set(df_methods[df_methods.method == "X-RAY DIFFRACTION"].PDB_ID.values)
solid_state_nmr_ids = set(
    df_methods[df_methods.method == "SOLID-STATE NMR"].PDB_ID.values
)
solution_nmr_ids = set(df_methods[df_methods.method == "SOLUTION NMR"].PDB_ID.values)
selected_ids = xray_ids.union(solid_state_nmr_ids, solution_nmr_ids)

with open("PDB_PAIRS.TSV", "w+") as f:
    for i, row in df_hi_pid.iterrows():
        p1 = row.qid.split("_")
        p2 = row.tid.split("_")

        id1 = p1[0]
        id2 = p2[0]

        # Only write structures from Xray experiments
        if (id1 in selected_ids) and (id2 in selected_ids):
            subchain1 = p1[1]
            subchain2 = p2[1]

            f.write("{}\t{}\t{}\t{}\n".format(id1, subchain1, id2, subchain2))