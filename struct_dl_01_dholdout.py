#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import numpy as np
import pandas as pd
import pickle
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # or any {'0', '1', '2'}

import matplotlib.pyplot as plt
import time
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import (
    Conv1D,
    ZeroPadding2D,
    Activation,
    Input,
    concatenate,
)
from tensorflow.keras.models import Model
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import GlobalMaxPool1D, GlobalAvgPool1D, MaxPooling1D
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import Lambda, Flatten, Dense, ReLU
from tensorflow.keras.initializers import glorot_uniform
from tensorflow.keras.layers import Layer
from tensorflow.keras.regularizers import l2
from tensorflow.keras.utils import Sequence
from tensorflow.keras import activations
import tensorflow.keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import numpy.random as rng
from common_funcs_2 import pad_seqs_to_onehot
import common_funcs_2 as cf
import argparse
import json
import utils
import model_def


"""
........................................................................................
    Main
........................................................................................
"""
physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs detected:", len(physical_devices))

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument(
    "-p",
    "--checkpoint",
    nargs="?",
    const="",
    help="timestamp of checkpoint to load.",
)

parser.add_argument(
    "-e",
    "--epochs",
    nargs="?",
    const=5,
    default=5,
    type=int,
    help="Epochs",
)

parser.add_argument(
    "-c",
    "--config",
    nargs="?",
    const="config.json",
    default="config.json",
    help="name of the config file ",
)

args = parser.parse_args()

with open(args.config, "r") as f:
    p = json.load(f)

time_stamp = cf.date_stamp()
p["date_time"] = time_stamp
p["config_file_name"] = "save/configs/" + p["date_time"] + ".json"

SCRATCH_PATH = p["SCRATCH_PATH"]
DATA_PATH = SCRATCH_PATH + "/ML/struct/"
SAVE_PLOTS_DIR = SCRATCH_PATH + "/ML/struct/save/plots/"
SAVE_WEIGHTS_DIR = SCRATCH_PATH + "/ML/struct/save/weights/"
CHECKPOINT_DIR = SCRATCH_PATH + "/ML/struct/save/chkpt/"


"""
   Read the training df
"""
df_scores_all = pd.read_pickle(DATA_PATH + "SCORES.pkl.gz")

FILTERING = True
max_tm_scores = np.maximum(df_scores_all["TM1"], df_scores_all["TM2"])
df_scores_all["distance"] = 1 - max_tm_scores
max_seq_len = p["MAX_SEQ_LEN"]
print("Discarding pairs >{} residues, and TMscore>{:.2f}".format(max_seq_len, p["TMSCORE_CUTOFF"]))

# Select the training set.
length_filter = (df_scores_all.qlen <= max_seq_len) & (
    df_scores_all.tlen <= max_seq_len
)
tm_score_filter = df_scores_all.distance < (1 - p["TMSCORE_CUTOFF"])

train_filter = length_filter & tm_score_filter & (df_scores_all.train_or_holdout == 0)
double_HO_filter = length_filter & tm_score_filter & (df_scores_all.train_or_holdout == 3)

df_train_and_val = df_scores_all[train_filter]
df_double_holdout = df_scores_all[double_HO_filter]
df_train, df_validation = train_test_split(
    df_train_and_val, test_size=0.05, random_state=425364
)

print("Training set size:", len(df_train))
print("Holdout set size (double-holdouts):", len(df_double_holdout))
print("Validation set (no-holdouts)", len(df_validation))
p["n_selected_sequences"] = len(df_train_and_val) + len(df_double_holdout)
p["n_validation_sequences"] = len(df_double_holdout)
p["n_training_sequences"] = len(df_train_and_val)
p["n_single_holdout_sequences"] = -1
p["n_double_holdout_sequences"] = len(df_double_holdout)
# df_train_and_val = df_train_and_val.sample(100000, random_state=424242)


log_dir = "save/tboard/" + time_stamp
checkpoint_path = CHECKPOINT_DIR + time_stamp + ".ckpt"

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

cp_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path, save_weights_only=True, verbose=1
)


if not FILTERING:
    nplen = np.vectorize(len)
    all_seqs = np.append(df_train_and_val.qseq.values, df_train_and_val.tseq.values)
    max_seq_len = np.max(nplen(all_seqs))

padded_len = max_seq_len + 10


model = model_def.get_siamese_model(padded_len)
optimizer = Adam(lr=0.0005)
model.compile(loss="mse", optimizer=optimizer, metrics=['mae'])

if args.checkpoint:
    checkpoint_path = CHECKPOINT_DIR + args.checkpoint + ".ckpt"
    model.load_weights(checkpoint_path)
    print("Loadaed checkpint", args.checkpoint)
    print("Running for ", args.epochs, "epochs.")

epochs = args.epochs
p["epochs"] = epochs

train_gen = model_def.DataGenerator(df_train_and_val, padded_len, max_seq_len, batch_size=128)
validation_gen = model_def.DataGenerator(df_double_holdout, padded_len, max_seq_len, batch_size=128)


"""
    Train
"""

history = model.fit(
    train_gen,
    epochs=epochs,
    verbose=1,
    use_multiprocessing=False,
    validation_data=validation_gen,
    callbacks=[
        tensorboard_callback,
        cp_callback,
    ],
)

"""


"""

model.save_weights(SAVE_WEIGHTS_DIR + time_stamp + "-weights.hdf5")

# HOLDOUT_SAMPLE_SIZE = 10000
# df_sample = df_double_holdout.sample(n=HOLDOUT_SAMPLE_SIZE)
# df_sample = df_double_holdout
#df_sample = df_validation.sample(5000)
df_sample = df_double_holdout
q_seqs = df_sample.qseq.values.tolist()
t_seqs = df_sample.tseq.values.tolist()
tmalign_distance = df_sample.distance

q_seqs_ohe = pad_seqs_to_onehot(
    q_seqs, padded_len, max_seq_len, random_padding=False, center_padding=True, dtype=np.int8,
)

t_seqs_ohe = pad_seqs_to_onehot(
    t_seqs, padded_len, max_seq_len, random_padding=False, center_padding=True, dtype=np.int8,
)

# float16 greatly reduces df saving time.
start = time.time()
predicted_distance = np.array(model.predict([q_seqs_ohe, t_seqs_ohe]).flatten())

q_len = np.array([len(x) for x in q_seqs])
t_len = np.array([len(x) for x in t_seqs])
len_ratio = (q_len - t_len) / q_len
distance_ratio = tmalign_distance / predicted_distance

p = utils.plot_concordance(
    tmalign_distance,
    predicted_distance,
    distance_ratio,
    len_ratio,
    time_stamp,
    SAVE_PLOTS_DIR,
    p,
)
utils.plot_hist(history, p, val_loss=True, scale=None)
utils.save_config(p)
