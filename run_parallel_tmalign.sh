# Run tmalign on pairs. 
# For ensemble pairs, pick the pair with the lowest rmsd.

date=$(date '+%Y-%m-%d')
echo "Moving JOB.LOG, STRUCTURAL_ALIGNMENT_SCORES.TSV, STDERR.LOG to .backup/$date"
echo "Comment the following line out if continuing the run."
# mkdir -p .backup/$date
# mv JOB.LOG STRUCTURAL_ALIGNMENT_SCORES.TSV STDERR.LOG .backup/$date/

# ~/bin/parallel \
#     -a PDB_PAIRS.TSV \
#     --resume \
#     --joblog JOB.LOG \
#     --colsep '\t' \
#     timeout 10 ./TMalign cif/{1}.cif cif/{3}.cif \
# 		-S1 {2} -S2 {4} \
#         -split 2 -ter 0 -outfmt 2 -noheader \
#         >>STRUCTURAL_ALIGNMENT_SCORES.TSV 2>>STDERR.LOG

# Use parallel --timeout option instead of the shell timeout command 
~/bin/parallel \
    -a PDB_PAIRS.TSV \
    --resume \
    --joblog JOB.LOG \
    --colsep '\t' \
    --timeout 11 \
    ./TMalign cif/{1}.cif cif/{3}.cif \
		-S1 {2} -S2 {4} \
        -split 2 -ter 0 -outfmt 2 -noheader \
        >>STRUCTURAL_ALIGNMENT_SCORES.TSV 2>>STDERR.LOG
