#srun env -i HOME="$HOME" /bin/bash --noprofile --norc -c \
~/bin/parallel \
    -a dummy_100_pairs.tsv \
    --resume \
    --joblog debug_joblog.log \
    --colsep '\t' \
    ./TMalign cif/{1}.cif cif/{3}.cif \
        -S1 {2} -S2 {4} \
        -split 2 -ter 0 -outfmt 2 -noheader \
        >> debug_RESULTS.TXT 2>>debug_STDERR.LOG



