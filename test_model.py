#!/usr/bin/env python
# coding: utf-8

import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # or any {'0', '1', '2'}

import sys
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import time
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import (
    Conv1D,
    ZeroPadding2D,
    Activation,
    Input,
    concatenate,
)
from tensorflow.keras.models import Model
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import GlobalMaxPool1D, GlobalAvgPool1D, MaxPooling1D
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import Lambda, Flatten, Dense, ReLU
from tensorflow.keras.initializers import glorot_uniform
from tensorflow.keras.layers import Layer
from tensorflow.keras.regularizers import l2
from tensorflow.keras.utils import Sequence
from tensorflow.keras import activations
import tensorflow.keras.backend as K
from sklearn.utils import shuffle
import numpy.random as rng
from common_funcs_2 import pad_seqs_to_onehot
import common_funcs_2 as cf
import argparse
import json
import scipy
import model_def


"""
........................................................................................
    Main
........................................................................................
"""

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument(
    "-p",
    "--checkpoint",
    nargs="?",
    const="",
    required=True,
    help="timestamp of checkpoint to load.",
)

parser.add_argument(
    "-c",
    "--config",
    nargs="?",
    const="config.json",
    default="config.json",
    help="name of the config file ",
)

args = parser.parse_args()

with open(args.config, "r") as f:
    p = json.load(f)

time_stamp = args.checkpoint
DATA_PATH = "/global/homes/v/vsevim/scratch/ML/struct/"
SAVE_PLOTS_DIR = "/global/homes/v/vsevim/scratch/ML/struct/save/plots/"
SAVE_WEIGHTS_DIR = "/global/homes/v/vsevim/scratch/ML/struct/save/weights/"
CHECKPOINT_DIR = "/global/homes/v/vsevim/scratch/ML/struct/save/chkpt/"


"""
   Read the training df
"""
df_scores_all = pd.read_pickle(DATA_PATH + "SCORES.pkl.gz")

FILTERING = True
max_tm_scores = np.maximum(df_scores_all["TM1"], df_scores_all["TM2"])
df_scores_all["distance"] = 1 - max_tm_scores
max_seq_len = p["MAX_SEQ_LEN"]
print("Discarding pairs >{} residues.".format(max_seq_len))

# Select the training set.
length_filter = (df_scores_all.qlen <= max_seq_len) & (
    df_scores_all.tlen <= max_seq_len
)

train_filter = length_filter & (df_scores_all.train_or_holdout == 0)
single_HO_filter = length_filter & (
    (df_scores_all.train_or_holdout == 1) | (df_scores_all.train_or_holdout == 2)
)
double_HO_filter = length_filter & (df_scores_all.train_or_holdout == 3)

df_train_and_val = df_scores_all[train_filter]
df_single_holdout = df_scores_all[single_HO_filter]
df_double_holdout = df_scores_all[double_HO_filter]

print("Sequence set size (train+validation):", len(df_train_and_val))
# df_train_and_val = df_train_and_val.sample(100000, random_state=424242)


if not FILTERING:
    nplen = np.vectorize(len)
    all_seqs = np.append(df_train_and_val.qseq.values, df_train_and_val.tseq.values)
    max_seq_len = np.max(nplen(all_seqs))

padded_len = max_seq_len + 10

"""

"""

model = model_def.get_siamese_model(padded_len)
optimizer = Adam(lr=0.0005)
model.compile(loss="MSE", optimizer=optimizer)

"""
    Load weights
"""

if args.checkpoint:
    checkpoint_path = CHECKPOINT_DIR + args.checkpoint + ".ckpt"
    model.load_weights(checkpoint_path)
    print("Loaded checkpoint", args.checkpoint)

HOLDOUT_SAMPLE_SIZE = 50000
WHICH_SET = "Single-holdout"
df_sample = df_single_holdout.sample(n=HOLDOUT_SAMPLE_SIZE)
q_seqs = df_sample.qseq.values.tolist()
t_seqs = df_sample.tseq.values.tolist()
tmalign_distance = df_sample.distance

"""
    Converting sequences to one-hot representations
"""
print("Conveting to one-hot...")
q_seqs_ohe = pad_seqs_to_onehot(
    q_seqs, padded_len, max_seq_len, random_padding=False, center_padding=True
)

t_seqs_ohe = pad_seqs_to_onehot(
    t_seqs, padded_len, max_seq_len, random_padding=False, center_padding=True
)


"""
    
"""


# float16 greatly reduces df saving time.
print("Generating representations")
start = time.time()
predicted_distance = np.array(model.predict([q_seqs_ohe, t_seqs_ohe]).flatten())

q_len = np.array([len(x) for x in q_seqs])
t_len = np.array([len(x) for x in t_seqs])
len_ratio = (q_len - t_len) / q_len
distance_ratio = tmalign_distance / predicted_distance

print("Plotting...")
R, p = scipy.stats.pearsonr(tmalign_distance, predicted_distance)
title = "{}: Pearson R={:.2f}\n{}".format(WHICH_SET, R, time_stamp)

fig = plt.figure(figsize=(6, 6))
fig.patch.set_facecolor("white")
plt.axis((0.0, 1, 0.0, 1))
plt.scatter(tmalign_distance, predicted_distance, s=0.1)
plt.plot([0, 1], [0, 1], color="red")
plt.title(title)
plt.xlabel("Actual Distance")
plt.ylabel("Predicted Distance")
file_name = SAVE_PLOTS_DIR + time_stamp + "-TEST-SINGLEHO-actual_vs_predicted.png"
plt.savefig(file_name, format="png", pad_inches=0.3)


# In[ ]:


diff = np.array(tmalign_distance - predicted_distance)
mean = np.average(diff)
stdev = np.std(diff)
title = "{}: Mean={:.2f}, Stdev={:.2f}\n{}".format(WHICH_SET, mean, stdev, time_stamp)
fig = plt.figure(figsize=(6, 6))
fig.patch.set_facecolor("white")
# plt.axis((0.5, 1, 0.5, 1))
plt.hist(diff, bins=25)
plt.title(title)
plt.xlabel("tmalign - predicted distance")
plt.ylabel("Count")
file_name = SAVE_PLOTS_DIR + time_stamp + "-TEST-SINGLEHO-distance_deviation_dist.png"
plt.savefig(file_name, format="png", pad_inches=0.3)

print("R={:.2f}, mean={:.2f}, stdev={:.2f}".format(R, mean, stdev))
# In[ ]:


R, p = scipy.stats.pearsonr(distance_ratio, len_ratio)
title = "{}: Pearson R={:.2f}\n{}".format(WHICH_SET, R, time_stamp)
fig = plt.figure(figsize=(6, 6))
fig.patch.set_facecolor("white")

# plt.axis((0.5, 1, 0.5, 1))
plt.scatter(distance_ratio, len_ratio, s=0.1)
plt.title(title)
plt.xlabel("tmalign_distance / predicted_distance")
plt.ylabel("q_len/t_len")
file_name = SAVE_PLOTS_DIR + time_stamp + "-TEST-SINGLEHO-length_diff_vs_deviation.png"
plt.savefig(file_name, format="png", pad_inches=0.3)


# In[ ]:
