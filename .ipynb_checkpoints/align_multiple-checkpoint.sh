address="https://files.rcsb.org/download/"
TMcut=0

while read -r cif1 cif2; do
    if [[ -z $cif1 ]] || [[ -z $cif2 ]]; then
        #echo "$1"
        #echo USAGE: align_pairs.sh PDBid1 PDBid2 TMcut
        exit -2
    fi

    cif1="$cif1".cif
    cif2="$cif2".cif

    cif1="cif/$cif1"
    cif2="cif/$cif2"

    if [[ -f "$cif1" ]] && [[ -f "$cif2" ]]; then
        #echo running $cif1 $cif2
        ./TMalign  "$cif1" "$cif2" -split 2 -ter 0 -outfmt 2 -TMcut "$TMcut"
        if [[ $? != 0 ]]; then
            echo $cif1 $cif2 >&2
        fi
    else
        echo "#!! $cif1 or $cif2 not found. TMalign not run."
        exit -1
    fi
done