in_tsv="$1"
out_tsv="$2"
out_faa="$3"
unfiltered_tsv=$4
min_len="$5"
set -e 
sort -u -k2,2 -t$'\t' "$in_tsv"  | \
	awk -F'\t' '{print $1"\t"$2}'| \
    grep -Pv '\tA+$' \
    > "$unfiltered_tsv"

echo Removing redundancy...

awk_line='
{
    if((length($2)>MINLEN) && 
        ($2 ~ /^[G,A,L,M,F,W,K,Q,E,S,P,V,I,C,Y,H,R,N,D,T]+$/))  
    print $0;
}
'

awk -v MINLEN=$min_len "$awk_line" "$unfiltered_tsv" > "$out_tsv"

echo Redundancy removed, sequences filtered. 

awk '{print ">"$1"\n"$2}' "$out_tsv" > "$out_faa"  

