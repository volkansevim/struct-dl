cat STRUCTURAL_ALIGNMENT_SCORES.TSV | \
    tr ":" "\t" | sed 's/cif\///g' | \
    sed 's/\.cif//g' > "scores.dummy.tsv"

awk '{
    if(FILENAME == "pdb_seqres.tsv") {
        seqs[$1] = $2
        next
    }
    
    if(FILENAME == "scores.dummy.tsv")
    {
        pdb1 = $1"_"$3 
        pdb2 = $4"_"$6
        print pdb1"\t"pdb2"\t"seqs[pdb1]"\t"seqs[pdb2]
    }
} ' pdb_seqres.tsv scores.dummy.tsv > SEQ_PAIRS.TSV

