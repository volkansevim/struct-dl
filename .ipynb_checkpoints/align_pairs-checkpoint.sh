address="https://files.rcsb.org/download/"

if [[ -z $2 ]]; then
    #echo "$1"
	#echo USAGE: align_pairs.sh PDBid1 PDBid2 TMcut
	exit -2
fi

cif1="$1".cif
cif2="$2".cif

if [[ -z $3 ]]; then
    TMcut=0
else
    TMcut=$3
fi

#wget -q -nc -P "/dev/shm/" "$address""$cif1" 
#wget -q -nc -P "/dev/shm/" "$address""$cif2" 

cif1="/dev/shm/$cif1"
cif2="/dev/shm/$cif2"

if [[ -f "$cif1" ]] && [[ -f "$cif2" ]]; then
    #echo running $cif1 $cif2
    ./TMalign  "$cif1" "$cif2" -split 2 -ter 0 -outfmt 2 -TMcut "$TMcut" 
else
    echo "#!! $cif1 or $cif2 not found. TMalign not run."
    exit -1
fi

#parallel -a p.tsv --colsep ' ' ./align_pairs.sh {} > Results.tsv 