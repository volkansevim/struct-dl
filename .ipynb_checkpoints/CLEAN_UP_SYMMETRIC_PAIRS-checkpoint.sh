infile=$1

awkline='
{
    if($1>$2)
        print $2"\t"$1"\tR\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11;
    else 
        print $1"\t"$2"\tF\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11;
}
'

awk "$awkline" "$infile"