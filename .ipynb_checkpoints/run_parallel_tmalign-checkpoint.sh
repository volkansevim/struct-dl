~/bin/parallel \
    -a PDB_PAIRS.TSV \
    --resume \
    --joblog joblog.log \
    --colsep '\t' \
    ./TMalign cif/{1}.cif cif/{3}.cif \
		-S1 {2} -S2 {4} \
        -split 2 -ter 0 -outfmt 2 \
        >> RESULTS.TXT 2>>STDERR.LOG
