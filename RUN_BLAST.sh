module load blast+
NUM_THREADS=$3
faa=$1
output=$2

echo "Blasting $faa"
blastp  -db ./blastdb/pdb \
		-query "$faa" \
		-out "$output" \
		-outfmt "7 qseqid sseqid pident length mismatch qlen slen qcov qcovhsp evalue bitscore" \
        -max_hsps 1 \
		-evalue 1e-6 \
		-num_threads $NUM_THREADS

echo "Blast completed."