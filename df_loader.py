import pandas as pd
import numpy as np


def loader(p):
    DATA_PATH = p["SCRATCH_PATH"] + "/ML/struct/"

    df_scores_all = pd.read_pickle(DATA_PATH + p["DF_NAME"])
    max_tm_scores = np.maximum(df_scores_all["TM1"], df_scores_all["TM2"])
    df_scores_all["distance"] = 1 - max_tm_scores
    exponent = p["SAMPLE_WEIGHT_SCALE_EXPONENT"]
    df_scores_all["weight"] = (1 - df_scores_all["pidentity"] / 100) ** exponent

    max_seq_len = p["MAX_SEQ_LEN"]
    print(
        "Discarding pairs >{} residues, and BLAST %ID>{:.2f}".format(
            max_seq_len, p["BLAST_PIDENTITY_CUTOFF"]
        )
    )
    if p["WEIGHTED_SAMPLES"]:
        print("Weighting the samples. Scale =", p["SAMPLE_WEIGHT_SCALE_EXPONENT"])

    # Select the training set.
    length_filter = (df_scores_all.qlen <= max_seq_len) & (
        df_scores_all.tlen <= max_seq_len
    )
    tm_score_filter = df_scores_all.distance > (1 - p["TMSCORE_CUTOFF"])
    pidentity_filter = df_scores_all.pidentity < p["BLAST_PIDENTITY_CUTOFF"]

    train_filter = (
        length_filter
        & tm_score_filter
        & pidentity_filter
        & (df_scores_all.train_or_holdout == 0)
    )
    double_HO_filter = (
        length_filter & tm_score_filter & (df_scores_all.train_or_holdout == 3)
    )

    df_train_and_val = df_scores_all[train_filter]
    df_double_holdout = df_scores_all[double_HO_filter]

    return df_train_and_val, df_double_holdout
