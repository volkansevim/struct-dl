#!/bin/bash
#SBATCH -N 1
#SBATCH -C haswell
#SBATCH -q debug
#SBATCH -t 00:03:00
#SBATCH --exclusive
#SBATCH -J TMExclusive

srun env -i HOME="$HOME" \
    /bin/bash --noprofile --norc \
    -c ./run_parallel_tmalign.sh
