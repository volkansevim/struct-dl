import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import argparse
import json
import sys

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument(
    "-c",
    "--config",
    nargs="?",
    const="config.json",
    default="config.json",
    help="name of the config file ",
)

args = parser.parse_args()

with open(args.config, "r") as f:
    p = json.load(f)

col_names = (
    "pdbid1",
    "model1",
    "chain1",
    "pdbid2",
    "model2",
    "chain2",
    "TM1",
    "TM2",
    "RMSD",
    "pID1",
    "pID2",
    "IDali",
    "L1",
    "L2",
    "Lali",
)

df_scores = pd.read_csv("scores.dummy.tsv", sep="\t", names=col_names)
print("{:,} total records in scores.dummy.tsv".format(len(df_scores)))
df_scores = df_scores.drop_duplicates()
print(
    "{:,} total records in scores.dummy.tsv after dropping duplicates".format(
        len(df_scores)
    )
)

df_seqs = pd.read_csv(
    "SEQ_PAIRS.TSV", sep="\t", names=("pdbid1", "pdbid2", "seq1", "seq2")
)

print("{:,} records in SEQ_PAIRS.TSV".format(len(df_seqs)))
print("Should be identical to scores.dummy count")

df_scores["qseq"] = df_seqs["seq1"]
df_scores["tseq"] = df_seqs["seq2"]

nplen = np.vectorize(len)
qlen = nplen(df_scores.qseq.values)
tlen = nplen(df_scores.tseq.values)

df_scores["qlen"] = qlen
df_scores["tlen"] = tlen


"""
    Even though I eliminated all NMR experiments, a small number of XRAY ones 
    also contain multiple models. I eliminate those by picking only model #1 
    from all pdb IDs.
"""
df_scores = df_scores[(df_scores.model1 == 1) & (df_scores.model2 == 1)]
print("{:,} records in df_scores after eliminating model>1".format(len(df_scores)))


""" 
    Create new rows with complete pdb ids: 2pk4 + A --> 2pk4_A
"""
id1 = df_scores.pdbid1.values
id2 = df_scores.pdbid2.values

chain1 = df_scores.chain1.values
chain2 = df_scores.chain2.values

zipped1 = zip(id1, chain1)
zipped2 = zip(id2, chain2)

compound_pdbid1 = [x + "_" + y for x, y in zipped1]
compound_pdbid2 = [x + "_" + y for x, y in zipped2]

df_scores["q_compound_pdbid"] = compound_pdbid1
df_scores["t_compound_pdbid"] = compound_pdbid2


"""
    Get blast %ids from into the scroes df. 
"""
blast_df = pd.read_pickle("HIGH_SEQ_ID_PAIRS.pkl.gz")
print("Joining blast scores with tm-align scores...")

df_scores = df_scores.merge(
    blast_df,
    how="left",
    left_on=["q_compound_pdbid", "t_compound_pdbid"],
    right_on=["qid", "tid"],
    suffixes=("_tm", None),
)

df_scores = df_scores.drop(columns=["q_compound_pdbid", "t_compound_pdbid"])

"""
    Mark ordered pairs ie, qlen<tlen (complete sequence lengths used in blast 
    comparison). If qlen==tlen, then check if qid<tid.

    Keep only ordered pairs.
"""

pairs = [
    qid + tid if qid < tid else tid + qid
    for qid, tid in df_scores[["qid", "tid"]].values
]
df_scores["pair"] = pairs

df_scores = df_scores.sort_values("qid").groupby(["pair"]).first().reset_index()
len_df = len(df_scores)

# 'pair' column is not needed anymore.
df_scores = df_scores.drop(columns=["pair"])
print("{:,} pairs".format(len_df))
print("Keeping (A,B) pairs, discarding (B,A)...")

"""
    -- Select train/holdout sets --
    Seqs in the holdout set shouldn't bear high %ID to training sequences. In order
    to avoid that problem, I first filter out PDBIDs with >96% maximum %id to any other 
    sequence in the set. Then I sample from the remaining seqs to get a more balanced
    set of holdout set. See viz_training_holdout_data.ipynb.

    train_or_holdout = 0: Training
    train_or_holdout = 1: single-holdout: only query is held out
    train_or_holdout = 2: single-holdout: only target is held out
    train_or_holdout = 0: double-holdout: both query+target are held out

    Either use 1|2, or 3 when masking. 
"""
# Picked MAX_PIDENTITY after visual inspection.
MAX_PIDENTITY = p["MAX_PIDENTITY_PER_CHAIN_FOR_HOLDOUT_SET"]
HOLDOUT_FRACTION = p["HOLDOUT_FRACTION"]

# These are all PDBIDs with TM-align scores.
all_ids = np.append(df_scores.qid.unique(), df_scores.tid.unique())
all_ids = set(all_ids)

# This is the set of largest %ids for each pdbid to another.
print("Groupby for holdout selection...")
df_max_pidentity_for_each_pdbid = df_scores[["qid", "pidentity"]].groupby("qid").max()

# Take only pdbids with largest %id < MAX_PIDENTITY
print(
    "Groupby complete. Filtering out PDBIDs above {}% maximum %id".format(MAX_PIDENTITY)
)
pidentity_filter = df_max_pidentity_for_each_pdbid.pidentity <= MAX_PIDENTITY

# selected_ids are for picking the holdouts
selected_ids = df_max_pidentity_for_each_pdbid[pidentity_filter].index.values
print("{:,}/{:,} chains available as holdouts".format(len(selected_ids), len(all_ids)))

#
HOLDOUT_COUNT = int(len(all_ids) * HOLDOUT_FRACTION)
assert HOLDOUT_COUNT < len(selected_ids), "Assert fail:HOLDOUT_COUNT>len(selected_ids)!"
holdout_ids = set(np.random.choice(selected_ids, size=HOLDOUT_COUNT, replace=False))
#holdout_ids = set(selected_ids)

print("Keeping all possible holdouts...")
training_ids = all_ids - holdout_ids

print(
    "{:,} total chains\n{:,} in training set\n{:,} in holdout_set".format(
        len(all_ids), len(training_ids), len(holdout_ids)
    )
)

qid = df_scores.qid.values
tid = df_scores.tid.values

qid_filter = np.array([x in holdout_ids for x in qid])
tid_filter = np.array([x in holdout_ids for x in tid])

df_scores["train_or_holdout"] = 0
df_scores.loc[qid_filter, "train_or_holdout"] = 1
df_scores.loc[tid_filter, "train_or_holdout"] = 2
df_scores.loc[qid_filter & tid_filter, "train_or_holdout"] = 3


"""
    Save to df
"""
print("Saving SCORES.pkl.gz")
df_scores.to_pickle("SCORES.pkl.gz")
